#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>

#include "app.h"
#include "target.h"


#define _APP_DLIST_HEAD( List )         ( ( List )->Head )

#define _APP_DLIST_SIZE( List )         ( ( List )->Size )

#define _APP_QUEUE_HEAD( Queue )        ( ( Queue )->Head )

#define _APP_QUEUE_SIZE( Queue )        ( ( Queue )->Size )


static void _app_dlistDestroy( APP_DLIST * List );

static void _app_dlistInitialise( APP_DLIST * List, void * Target );

static int _app_dlistInsert( APP_DLIST * List, const void * Data );

static int _app_dlistRemove( APP_DLIST * List, APP_DLIST_ELEMENT * Element, void ** Data );

static void _app_processHandles( APP * App, struct timeval * Timeout );

static int _app_queueCompare( void * Left, void * Right );

static void _app_queueDestroy( APP_QUEUE * Queue );

static void _app_queueInitialise( APP_QUEUE * Queue, APP_QUEUE_COMPARE Compare, void * Target );

static int _app_queueInsert( APP_QUEUE * Queue, const void * Data );

static int _app_queueNext( APP_QUEUE * Queue, void ** Data );

static int _app_queueRemove( APP_QUEUE * Queue, int Id, void ** Data );


static void
_app_dlistDestroy( APP_DLIST * List )
{
    void *pData;


    /* assert( List != NULL ); */
    while( _APP_DLIST_SIZE( List ) > 0 )
    {
        if( _app_dlistRemove( List, _APP_DLIST_HEAD( List ), &pData ) == 0 )
        {
            target_free( List->Target, pData );
        }
    }
    List->Target = NULL;
}


static void
_app_dlistInitialise( APP_DLIST * List, void * Target )
{
	/* assert( List != NULL ); */
    List->Head = NULL;
    List->Tail = NULL;
    List->Size = 0;
    List->Target = Target;
}


static int
_app_dlistInsert( APP_DLIST * List, const void * Data )
{
    APP_DLIST_ELEMENT *pElement, *pNew;


    /* assert( List != NULL ); */
    if( ( pNew = ( APP_DLIST_ELEMENT * ) target_alloc( List->Target, sizeof( *pNew ) ) ) == NULL )
    {
        return -errno;
    }
    pNew->Data = ( void * ) Data;
    pNew->Next = NULL;
    pNew->Previous = NULL;

    if( List->Head == NULL )
    {
        List->Head = pNew;
        List->Head->Previous = NULL;
        List->Head->Next = NULL;
        List->Tail = pNew;
    }
    else
    {
        pElement = List->Tail;
        pElement->Next = pNew;
        pNew->Previous = pElement;
        List->Tail = pNew;
    }

    ++List->Size;
    return 0;
}


static int
_app_dlistRemove( APP_DLIST * List, APP_DLIST_ELEMENT * Element, void ** Data )
{
    if( ( List == NULL ) ||
    		( Element == NULL ) )
    {
        return -EINVAL;
    }

    if( List->Size == 0 )
    {
        return -ENOENT;
    }

    *Data = Element->Data;
    if( Element == List->Head )
    {
        List->Head = Element->Next;
        if( List->Head == NULL )
        {
            List->Tail = NULL;
        }
        else
        {
            Element->Next->Previous = NULL;
        }
    }
    else
    {
        Element->Previous->Next = Element->Next;
        if( Element->Next == NULL )
        {
            List->Tail = Element->Previous;
        }
        else
        {
            Element->Next->Previous = Element->Previous;
        }
    }
    target_free( List->Target, Element );

    --List->Size;
    return 0;
}


static void
_app_processHandles( APP * App, struct timeval * Timeout )
{
    APP_DLIST_ELEMENT *pElement, *pNext;
    APP_HANDLE *pHandle;
    fd_set stReadHandles, stWriteHandles;
    void *pReadHandles, *pWriteHandles;
    int nMaximum, nResult;


    nMaximum = -1;
    FD_ZERO( &stReadHandles );
    FD_ZERO( &stWriteHandles );
    pReadHandles = NULL;
    pWriteHandles = NULL;

    if( _APP_DLIST_SIZE( &App->Handles ) > 0 )
    {
        for( pElement = _APP_DLIST_HEAD( &App->Handles ), pNext = NULL;
                pElement;
                pElement = pNext )
        {
            pNext = pElement->Next;
            pHandle = ( APP_HANDLE * ) pElement->Data;
            if( ( pHandle == NULL ) ||
                    ( pHandle->Handle < 0 ) )
            {
                continue;
            }

            if( pHandle->ReadCallback != NULL )
            {
                FD_SET( pHandle->Handle, &stReadHandles );
                nMaximum = ( nMaximum > pHandle->Handle ) ? nMaximum : pHandle->Handle;
                pReadHandles = &stReadHandles;
            }
            if( pHandle->WriteCallback != NULL )
            {
                FD_SET( pHandle->Handle, &stWriteHandles );
                nMaximum = ( nMaximum > pHandle->Handle ) ? nMaximum : pHandle->Handle;
                pWriteHandles = &stWriteHandles;
            }
        }
    }

    if( ( ( nResult = select( nMaximum + 1, pReadHandles, pWriteHandles, NULL, Timeout ) ) < 0 ) &&
            ( errno != EINTR ) )
    {
        App->Exit = 1;
        App->ExitCode = -errno;

        return;
    }
    else if( nResult <= 0 )
    {
        return;
    }

    for( pElement = _APP_DLIST_HEAD( &App->Handles ), pNext = NULL;
            pElement;
            pElement = pNext )
    {
        pNext = pElement->Next;
        pHandle = ( APP_HANDLE * ) pElement->Data;
        if( ( pHandle == NULL ) ||
                ( pHandle->Handle < 0 ) )
        {
            continue;
        }

        if( ( FD_ISSET( pHandle->Handle, &stReadHandles ) != 0 ) &&
                ( pHandle->ReadCallback != NULL ) )
        {
            ( pHandle->ReadCallback )( App, pHandle->Handle, pHandle->ReadParameter );
        }
        if( ( FD_ISSET( pHandle->Handle, &stWriteHandles ) != 0 ) &&
                ( pHandle->WriteCallback != NULL ) )
        {
            ( pHandle->WriteCallback )( App, pHandle->Handle, pHandle->WriteParameter );
        }
    }
}


static int
_app_queueCompare( void * Left, void * Right )
{
    APP_TIMER *pTimer1, *pTimer2;


    pTimer1 = ( APP_TIMER * ) Left;
    /* assert( pTimer1 != NULL ); */
    pTimer2 = ( APP_TIMER * ) Right;
    /* assert( pTimer2 != NULL ); */

    if( ( pTimer1->Expiry.tv_sec < pTimer2->Expiry.tv_sec ) ||
            ( ( pTimer1->Expiry.tv_sec == pTimer2->Expiry.tv_sec ) &&
                    ( pTimer1->Expiry.tv_usec < pTimer2->Expiry.tv_usec ) ) )
    {
        return -1;
    }
    else if( ( pTimer1->Expiry.tv_sec > pTimer2->Expiry.tv_sec ) ||
            ( ( pTimer1->Expiry.tv_sec == pTimer2->Expiry.tv_sec ) &&
                    ( pTimer1->Expiry.tv_usec > pTimer2->Expiry.tv_usec ) ) )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


static void
_app_queueDestroy( APP_QUEUE * Queue )
{
    void *pData;


    /* assert( Queue != NULL ); */
    while( _app_queueNext( Queue, &pData ) == 0 )
    {
        target_free( Queue->Target, pData );
    }
    Queue->Target = NULL;
}


static void
_app_queueInitialise( APP_QUEUE * Queue, APP_QUEUE_COMPARE Compare, void * Target )
{
	/* assert( Queue != NULL ); */
    Queue->Compare = Compare;
    Queue->Head = NULL;
    Queue->Tail = NULL;
    Queue->Size = 0;
    Queue->Id = 0;
    Queue->Target = Target;
}


static int
_app_queueInsert( APP_QUEUE * Queue, const void * Data )
{
    APP_QUEUE_ELEMENT *pElement, *pNew, *pNext;


    /* assert( Queue != NULL ); */
    if( ( pNew = ( APP_QUEUE_ELEMENT * ) target_alloc( Queue->Target, sizeof( *pNew ) ) ) == NULL )
    {
        return -errno;
    }
    pNew->Id = ++Queue->Id;
    pNew->Data = Data;

    if( Queue->Head == NULL )
    {
        Queue->Head = pNew;
        Queue->Head->Previous = NULL;
        Queue->Head->Next = NULL;
        Queue->Tail = pNew;
    }
    else if( Queue->Compare( pNew, Queue->Head ) < 0 )
    {
        pElement = Queue->Head;
        pNew->Next = pElement;
        pNew->Previous = NULL;
        pElement->Previous = pNew;
        Queue->Head = pNew;
    }
    else if( Queue->Compare( pNew, Queue->Tail ) >= 0 )
    {
        pElement = Queue->Tail;
        pNew->Previous = pElement;
        pNew->Next = NULL;
        pElement->Next = pNew;
        Queue->Tail = pNew;
    }
    else
    {
        for( pElement = Queue->Head, pNext = NULL;
                pElement;
                pElement = pNext )
        {
            pNext = pElement->Next;
            if( Queue->Compare( pNew, pElement ) > 0 )
            {
                continue;
            }

            if( pElement->Previous != NULL )
            {
                pElement->Previous->Next = pNew;
            }
            pNew->Previous = pElement->Previous;
            pNew->Next = pElement;
            pElement->Previous = pNew;

            break;
        }
    }

    ++Queue->Size;
    return Queue->Id;
}


static int
_app_queueNext( APP_QUEUE * Queue, void ** Data )
{
    APP_QUEUE_ELEMENT *pElement;


    /* assert( Queue != NULL ); */
    if( ( pElement = Queue->Head ) == NULL )
    {
        return -ENOENT;
    }
    *Data = ( void * ) pElement->Data;
    Queue->Head = pElement->Next;

    if( Queue->Head == NULL )
    {
        Queue->Tail = NULL;
    }
    else
    {
        Queue->Head->Previous = NULL;
    }

    target_free( Queue->Target, pElement );

    --Queue->Size;
    return 0;
}


static int
_app_queueRemove( APP_QUEUE * Queue, int Id, void ** Data )
{
    APP_QUEUE_ELEMENT *pElement, *pNext;


    /* assert( Queue != NULL ); */
    for( pElement = Queue->Head, pNext = NULL;
            pElement;
            pElement = pNext )
    {
        pNext = pElement->Next;
        if( pElement->Id == ( unsigned int ) Id )
        {
            break;
        }
    }
    if( pElement == NULL )
    {
        return -ENOENT;
    }

    *Data = ( void * ) pElement->Data;
    if( Queue->Head == pElement )
    {
        Queue->Head = pElement->Next;
        if( pElement->Next != NULL )
        {
            pElement->Next->Previous = NULL;
        }
    }
    else if( Queue->Tail == pElement )
    {
        Queue->Tail = pElement->Previous;
        if( pElement->Previous != NULL )
        {
            pElement->Previous->Next = NULL;
        }
    }
    else
    {
        pElement->Previous->Next = pElement->Next;
        pElement->Next->Previous = pElement->Previous;
    }

    target_free( Queue->Target, pElement );

    --Queue->Size;
    return 0;
}


int
app_addReadCallback( APP * App, int Handle, APP_HANDLE_CALLBACK Callback, void * Parameter )
{
    APP_DLIST_ELEMENT *pElement, *pNext;
    APP_HANDLE *pHandle;


    pElement = NULL;
    pHandle = NULL;

    if( App->Handles.Size > 0 )
    {
        for( pElement = App->Handles.Head, pNext = NULL;
                pElement;
                pElement = pNext )
        {
            pNext = pElement->Next;
            pHandle = ( APP_HANDLE * ) pElement->Data;
            if( pHandle == NULL )
            {
                return -EFAULT;
            }

            if( pHandle->Handle != Handle )
            {
                continue;
            }
            if( pHandle->ReadCallback != NULL )
            {
                return -EEXIST;
            }

            break;
        }
    }

    if( pElement == NULL )
    {
        if( ( pHandle = ( APP_HANDLE * ) target_alloc( App->Target, sizeof( *pHandle ) ) ) == NULL )
        {
            return -errno;
        }

        pHandle->ReadCallback = NULL;
        pHandle->WriteCallback = NULL;
    }

    pHandle->Handle = Handle;
    pHandle->ReadCallback = Callback;
    pHandle->ReadParameter = Parameter;

    return ( pElement == NULL ) ? _app_dlistInsert( &App->Handles, pHandle ) : 0;
}


int
app_addTimer( APP * App, unsigned int Interval, APP_TIMER_CALLBACK Callback, void * Parameter )
{
    APP_TIMER *pTimer;
    int nResult;


    /* assert( App != NULL ); */

    if( ( pTimer = ( APP_TIMER * ) target_alloc( App->Target, sizeof( *pTimer ) ) ) == NULL )
    {
        return -errno;
    }
    pTimer->Callback = Callback;
    pTimer->Parameter = Parameter;

    target_getSystemTime( App->Target, &pTimer->Expiry );

    pTimer->Expiry.tv_usec += ( Interval * 1000 );
    while( pTimer->Expiry.tv_usec > 1000000 )
    {
        pTimer->Expiry.tv_usec -= 1000000;
        ++pTimer->Expiry.tv_sec;
    }

    if( ( nResult = _app_queueInsert( &App->Timers, pTimer ) ) < 0 )
    {
        target_free( App->Target, pTimer );
        return nResult;
    }

    pTimer->Id = nResult;
    return nResult;
}


int
app_addWriteCallback( APP * App, int Handle, APP_HANDLE_CALLBACK Callback, void * Parameter )
{
    APP_DLIST_ELEMENT *pElement, *pNext;
    APP_HANDLE *pHandle;


    /* assert( App != NULL ); */

    pElement = NULL;
    pHandle = NULL;

    if( App->Handles.Size > 0 )
    {
        for( pElement = App->Handles.Head, pNext = NULL;
                pElement;
                pElement = pNext )
        {
            pNext = pElement->Next;
            pHandle = ( APP_HANDLE * ) pElement->Data;
            if( pHandle == NULL )
            {
                return -EFAULT;
            }

            if( pHandle->Handle != Handle )
            {
                continue;
            }
            if( pHandle->WriteCallback != NULL )
            {
                return -EEXIST;
            }

            break;
        }
    }

    if( pElement == NULL )
    {
        if( ( pHandle = ( APP_HANDLE * ) target_alloc( App->Target, sizeof( *pHandle ) ) ) == NULL )
        {
            return -errno;
        }

        pHandle->ReadCallback = NULL;
        pHandle->WriteCallback = NULL;
    }

    pHandle->Handle = Handle;
    pHandle->WriteCallback = Callback;
    pHandle->WriteParameter = Parameter;

    return ( pElement == NULL ) ? _app_dlistInsert( &App->Handles, pHandle ) : 0;
}


int
app_adjustTimer( APP * App, unsigned int Id, unsigned int Interval )
{
    APP_TIMER *pTimer;
    struct timeval stTime;
    int nResult;


    target_getSystemTime( App->Target, &stTime );

    if( ( nResult = _app_queueRemove( &App->Timers, Id, ( void ** ) &pTimer ) ) != 0 )
    {
        return nResult;
    }

    stTime.tv_usec += ( Interval * 1000 );
    while( stTime.tv_usec > 1000000 )
    {
        stTime.tv_usec -= 1000000;
        ++stTime.tv_sec;
    }
    pTimer->Expiry.tv_sec = stTime.tv_sec;
    pTimer->Expiry.tv_usec = stTime.tv_usec;

    if( ( nResult = _app_queueInsert( &App->Timers, pTimer ) ) < 0 )
    {
        target_free( App->Target, pTimer );
    }

    return nResult;
}


int
app_deleteReadCallback( APP * App, int Handle, void ** Parameter )
{
    APP_DLIST_ELEMENT *pElement, *pNext;
    APP_HANDLE *pHandle;
    void *pData;
    int nResult;


    /* assert( App != NULL ); */

    nResult = -ENOENT;
    if( App->Handles.Size == 0 )
    {
        return nResult;
    }

    for( pElement = App->Handles.Head, pNext = NULL;
            pElement;
            pElement = pNext )
    {
        pNext = pElement->Next;
        pHandle = ( APP_HANDLE * ) pElement->Data;
        if( pHandle == NULL )
        {
            return -EFAULT;
        }

        if( pHandle->Handle != Handle )
        {
            continue;
        }
        if( pHandle->ReadCallback != NULL )
        {
            pHandle->ReadCallback = NULL;
            *Parameter = pHandle->ReadParameter;

            nResult = 0;
            break;
        }

        break;
    }

    if( ( pElement != NULL ) &&
            ( pHandle->WriteCallback == NULL ) )
    {
        if( ( nResult = _app_dlistRemove( &App->Handles, pElement, ( void ** ) &pData ) ) == 0 )
        {
            target_free( App->Target, pData );
        }
    }

    return nResult;
}


int
app_deleteTimer( APP * App, unsigned int Id, void ** Parameter )
{
    APP_TIMER *pTimer;
    int nResult;


    /* assert( App != NULL ); */
    if( ( nResult = _app_queueRemove( &App->Timers, Id, ( void ** ) &pTimer ) ) == 0 )
    {
        *Parameter = pTimer->Parameter;
        target_free( App->Target, pTimer );
    }
    return nResult;
}


int
app_deleteWriteCallback( APP * App, int Handle, void ** Parameter )
{
    APP_DLIST_ELEMENT *pElement, *pNext;
    APP_HANDLE *pHandle;
    void *pData;
    int nResult;


    /* assert( App != NULL ); */

    nResult = -ENOENT;
    if( _APP_DLIST_SIZE( &App->Handles ) == 0 )
    {
        return nResult;
    }

    for( pElement = _APP_DLIST_HEAD( &App->Handles ), pNext = NULL;
            pElement;
            pElement = pNext )
    {
        pNext = pElement->Next;
        pHandle = ( APP_HANDLE * ) pElement->Data;
        if( pHandle == NULL )
        {
            return -EFAULT;
        }

        if( pHandle->Handle != Handle )
        {
            continue;
        }
        if( pHandle->WriteCallback != NULL )
        {
            pHandle->WriteCallback = NULL;
            *Parameter = pHandle->WriteParameter;

            nResult = 0;
            break;
        }

        break;
    }

    if( ( pElement != NULL ) &&
            ( pHandle->ReadCallback == NULL ) )
    {
        if( ( nResult = _app_dlistRemove( &App->Handles, pElement, ( void ** ) &pData ) ) == 0 )
        {
            if( pData != NULL )
            {
                target_free( App->Target, pData );
            }
        }
    }

    return nResult;
}


void
app_destroy( APP * App )
{
    _app_dlistDestroy( &App->Handles );
    _app_queueDestroy( &App->Timers );
}


void
app_exit( APP * App, int Exit )
{
    App->ExitCode = Exit;
    App->Exit = 1;
}


int
app_initialise( APP * App, void * Target )
{
    int nResult;


    if( ( nResult = target_initialise( Target ) ) != 0 )
    {
        return nResult;
    }
    App->Target = Target;

    _app_queueInitialise( &App->Timers, _app_queueCompare, App->Target );
    _app_dlistInitialise( &App->Handles , App->Target );

    App->Exit = 0;
    App->ExitCode = 0;

    return 0;
}


int
app_start( APP * App )
{
    APP_QUEUE_ELEMENT *pElement;
    APP_TIMER *pTimer;
    struct timeval stTime, stTimeout;
    long nTimeout;
    void *pData, *pTimeout;


    while( ( ( _APP_QUEUE_SIZE( &App->Timers ) > 0 ) ||
                    ( _APP_DLIST_SIZE( &App->Handles ) > 0 ) ) &&
            ( App->Exit == 0 ) )
    {
        pTimeout = NULL;

        if( ( pElement = _APP_QUEUE_HEAD( &App->Timers ) ) != NULL )
        {
            target_getSystemTime( App->Target, &stTime );

            pTimer = ( APP_TIMER * ) pElement->Data;
            nTimeout = ( ( pTimer->Expiry.tv_sec - stTime.tv_sec ) * 1000000 ) + ( pTimer->Expiry.tv_usec - stTime.tv_usec );

            if( nTimeout > 0 )
            {
                stTimeout.tv_sec = nTimeout / 1000000;
                stTimeout.tv_usec = nTimeout % 1000000;
            }
            else
            {
                stTimeout.tv_sec = 0;
                stTimeout.tv_usec = 0;
            }
            pTimeout = &stTimeout;
        }

        _app_processHandles( App, pTimeout );

        if( ( pElement = _APP_QUEUE_HEAD( &App->Timers ) ) != NULL )
        {
            target_getSystemTime( App->Target, &stTime );

            pTimer = ( APP_TIMER * ) pElement->Data;
            nTimeout = ( ( pTimer->Expiry.tv_sec - stTime.tv_sec ) * 1000000 ) + ( pTimer->Expiry.tv_usec - stTime.tv_usec );

            if( nTimeout <= 0 )
            {
                pData = NULL;

                ( void ) _app_queueNext( &App->Timers, ( void ** ) &pData );
                if( pData != NULL )
                {
                    pTimer = ( APP_TIMER * ) pData;
                    ( pTimer->Callback )( App, pTimer->Id, pTimer->Parameter );
                    target_free( App->Target, pTimer );
                }
            }
        }
    }

    return App->ExitCode;
}

