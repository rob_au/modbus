#ifndef __APP_H
#define __APP_H


#include <sys/time.h>


/*!
    \struct APP_DLIST_ELEMENT
    \brief Application double-linked list element data structure
*/

typedef struct _APP_DLIST_ELEMENT
{
    struct _APP_DLIST_ELEMENT * Previous;

    struct _APP_DLIST_ELEMENT * Next;

    void * Data;
}
APP_DLIST_ELEMENT;


/*!
    \struct APP_DLIST
    \brief Application double-linked list data structure
*/

typedef struct _APP_DLIST
{
    APP_DLIST_ELEMENT * Head;

    APP_DLIST_ELEMENT * Tail;

    unsigned int Size;

    void * Target;
}
APP_DLIST;


/*!
    \struct APP_QUEUE_ELEMENT
    \brief Application time-ordered queue element data structure
*/

typedef struct _APP_QUEUE_ELEMENT
{
    struct _APP_QUEUE_ELEMENT * Previous;

    struct _APP_QUEUE_ELEMENT * Next;

    unsigned int Id;

    struct timeval Time;

    const void * Data;
}
APP_QUEUE_ELEMENT;


typedef int ( * APP_QUEUE_COMPARE )( void * Left, void * Right );


/*!
    \struct APP_QUEUE
    \brief Application time-ordered queue data structure
*/

typedef struct _APP_QUEUE
{
    APP_QUEUE_ELEMENT * Head;

    APP_QUEUE_ELEMENT * Tail;

    APP_QUEUE_COMPARE Compare;

    unsigned int Size;

    unsigned int Id;

    void * Target;
}
APP_QUEUE;


/*!
    \struct APP
    \brief Application contextual data structure

    This data structure is intended to contain contextual information
    associated with application operations.  All members within this data
    structure should be considered reserved and private.
*/

typedef struct _APP
{
    APP_QUEUE Timers;

    APP_DLIST Handles;

    unsigned int Exit;

    int ExitCode;

    void * Target;
}
APP;


typedef void ( * APP_HANDLE_CALLBACK )( APP * App, int Handle, void * Parameter );

typedef void ( * APP_TIMER_CALLBACK )( APP * App, int Timer, void * Parameter );


/*!
    \struct APP_HANDLE
    \brief Application file handle callback data structure
*/

typedef struct _APP_HANDLE
{
    int Handle;

    APP_HANDLE_CALLBACK ReadCallback;

    void * ReadParameter;

    APP_HANDLE_CALLBACK WriteCallback;

    void * WriteParameter;
}
APP_HANDLE;


/*!
    \struct APP_TIMER
    \brief Application timer data structure
*/

typedef struct _APP_TIMER
{
    int Id;

    struct timeval Expiry;

    APP_TIMER_CALLBACK Callback;

    void * Parameter;
}
APP_TIMER;


int app_initialise( APP * App, void * Target );

void app_destroy( APP * App );

int app_start( APP * App );

void app_exit( APP * App, int Exit );

int app_addTimer( APP * App, unsigned int Interval, APP_TIMER_CALLBACK Callback, void * Parameter );

int app_adjustTimer( APP * App, unsigned int Id, unsigned int Interval );

int app_deleteTimer( APP * App, unsigned int Id, void ** Parameter );

int app_addReadCallback( APP * App, int Handle, APP_HANDLE_CALLBACK Callback, void * Parameter );

int app_addWriteCallback( APP * App, int Handle, APP_HANDLE_CALLBACK Callback, void * Parameter );

int app_deleteReadCallback( APP * App, int Handle, void ** Parameter );

int app_deleteWriteCallback( APP * App, int Handle, void ** Parameter );


#endif
