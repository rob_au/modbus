.PHONY: doc example lib

all: clean lib example doc

clean:
	rm -rf doc/*
	make -C lib $@
	make -C example $@

doc:
	doxygen Doxyfile > /dev/null 2>&1

example:
	make -C example

lib:
	make -C lib

